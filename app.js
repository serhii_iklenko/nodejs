const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");
const morgan = require("morgan");

const filesPath = path.dirname("./") + "/api/files/";

app.use(morgan("tiny"));
app.use(express.json());
app.use((req, res, next) => {
    console.log(req.url);
    next();
})
app.get('/', (req,res) => {
    res.end('Server is working...');
});

app.post("/api/files", (req, res) => {
    function checkRegExp(format) {
        return /.(log|txt|json|yaml|xml|js)$/g.test(format);
    }
    try {
        if (req.body.content && checkRegExp(path.extname(req.body.filename))) {
            fs.writeFile(filesPath + req.body.filename, req.body.content, () => {
                res.json({ message: "File created successfully" });
            });
        } else {
            throw new Error();
        }
    } catch (error) {
        res.status(400).json({ message: "Please specify 'content' parameter" })
    }
});

app.get("/api/files", (req, res) => {
    try {
        fs.readdir(filesPath, (err, files) => {
            res.json({ message: "Successful", files: files.filter(v => v !== "README.md") })
        })
    } catch (err) {
        res.status(400).json({ message: "Client error" })
    }
});

app.get("/api/files/:filename", (req, res) => {
    try {

        let result = {
            message: "Success",
            filename: "",
            content: "",
            extension: "",
            uploadedDate: "",
        };

        result.uploadedDate = fs.statSync(filesPath + req.params.filename).birthtime

        result.filename = req.params.filename;
        result.extension = path.extname(filesPath + req.params.filename).replace(".", "");

        fs.readFile(filesPath + req.params.filename, "utf8", (err, file) => {
            result.content = file
            res.json(result)
        })
    } catch (error) {
        res.status(400).json({ message: `No file with '${req.params.filename}' filename found` })
    }
});

app.use((req, res) => {
    res.status(500).json({ message: "Server error" })
});

app.listen(8080);

